simple cicd for flask application.

- setup python-flask local env
- setup gitlab runner in local desktop
- build image and push to dockerhub
- create ecs cluster from tf folder 
- create gitlab.yaml file 
- set env variable(option is available in gitlab setting) for store credentials 